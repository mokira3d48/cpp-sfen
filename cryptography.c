typedef unsigned short int usint;

usint cesar_encrypted_value( usint k,  usint x){
    return (x + k) % 65536;
}
usint cesar_decrypted_value( usint k,  usint x){
    return (x - k) % 65536;
}
