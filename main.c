#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

typedef unsigned short int usint;

usint cesar_encrypted_value( usint,  usint);
usint cesar_decrypted_value( usint,  usint);

void cesar_encrypted_values( usint k, int n, usint D[n]);
void cesar_decrypted_values(usint k, int n, usint D[n]);

void file_cesar_encryption(usint k, char *p1, char *p2);
void file_cesar_decryption(usint k, char *p1, char *p2);

void color(int x, int y);

int main(int argc, char *argv[]) {
    color(15, 0);

    int _choix;

    char c_e = (unsigned char)130;
    char c_a = (unsigned char)133;

    const int C_NMAX = 1000;

    char p1[C_NMAX];
    char p2[C_NMAX];
    usint k;

    _001:
    printf("Tapez 0 pour quitter l'application.\n");
    printf("Tapez 1 pour crypter un fichier.\n");
    printf("Tapez 2 pour d%ccrypter un fichier.\n\n", c_e);

    color(10, 0);

    printf("<Votre choix> ");
    scanf("%d", &_choix);

    printf("\n");

    color(15, 0);

    switch(_choix){
        case 0:
            return 0;
        case 1:
            gets(p1);//je ne sais pas pourquoi mais sans xa, xa ne marche pas correctement.

            printf("Entrer l'emplacement du fichier %c crypter.\n", c_a);

            color(10, 0);

            printf("<PATH> ");
            gets(p1);

            color(15, 0);

            printf("Entrer l'emplacement destinataire.");
            printf("Ex : D:/dossier/..../fichier.extension\n");

            color(10, 0);

            printf("<PATH> ");
            gets(p2);

            color(15, 0);

            printf("Entrer votre cl%c de cryptage de ce ficher (entre 0 et 65535).\n", c_e);

            color(10, 0);

            _002:printf("<KEY> ");
            scanf("%d", &_choix);

            if(_choix < 0 || _choix > 65535){
                color(12, 0);
                printf("C'est entre 0 et 65535, re%cssayez !", c_e);
                goto _002;

            }

            k = (usint) _choix;


            color(15, 0);

            file_cesar_encryption(k, p1, p2);
            break;

        case 2:
            gets(p1);//je ne sais pas pourquoi mais sans xa, xa ne marche pas correctement.

            printf("Entrer l'emplacement du fichier crypt%c.\n", c_e);

            color(10, 0);

            printf("<PATH> ");
            gets(p1);

            color(15, 0);

            printf("Entrer l'emplacement destinataire.");
            printf("Ex : D:/dossier/..../fichier.extension\n");

            color(10, 0);

            printf("<PATH> ");
            gets(p2);

            color(15, 0);

            printf("Entrer votre cl%c de d%ccryptage de ce ficher (entre 0 et 65535).\n", c_e, c_e);
            printf("Il s'agit de la m�me cl%c qui a %ct%c utilis%ce pour.\n", c_e, c_e, c_e, c_e);
            printf("crypter le fichier que vous voulez d%ccrypter.\n", c_e);

            color(10, 0);

            _003:printf("<KEY> ");
            scanf("%d", &_choix);

            if(_choix < 0 || _choix > 65535){
                color(12, 0);
                printf("C'est entre 0 et 65535, re%cssayez !", c_e);
                goto _003;

            }
            k = (usint) _choix;

            color(15, 0);

            file_cesar_decryption(k, p1, p2);
            break;

        default:
            color(9, 0);

            printf("Option indisponible pour le moment !\n\n");
            break;
    }

    color(15, 0);

    system("PAUSE");

    printf("\n");

    goto _001;

    return 0;
}

