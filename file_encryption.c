#include <stdio.h>
#include <windows.h>

typedef unsigned short int usint;

int BUFFER_SIZE = 1024;

usint cesar_encrypted_value( usint k,  usint x);
usint cesar_decrypted_value( usint k,  usint x);

void color(int x, int y);
void message(char* m, short int v);

void cesar_encrypted_values(usint k, int n, usint D[n]){
    int i = 0;
    for(i = 0; i < n; i++)
        D[i] = cesar_encrypted_value(k, D[i]);
}

void cesar_decrypted_values(usint k, int n, usint D[n]){
    int i = 0;
    for(i = 0; i < n; i++)
        D[i] = cesar_decrypted_value(k, D[i]);
}

void file_cesar_encryption(usint k, char *p1, char *p2){

    message("Opening two files ...", 0);

    FILE *f1 = fopen(p1, "rb");
    FILE *f2 = fopen(p2, "wb");

    message("Done !", 0);

    if(f1){

        message("Source file [OK]", 1);

        rewind(f1);

        if(f2){

            message("Destination file is created [OK]", 1);
            message("Encrypting file ...", 0);

            usint BUFFER[BUFFER_SIZE];
            int _length = 0;

            while((_length = fread(BUFFER, sizeof(usint), BUFFER_SIZE, f1)) > 0){
                cesar_encrypted_values(k, _length, BUFFER);
                fwrite(BUFFER, sizeof(usint), _length, f2);
            }

            message("Done !", 0);
        }
        else{
            message("Destination file is not created [ERROR]", 2);
        }
    }
    else{
        message("Source file [ERROR]", 2);
    }

    fclose(f1);
    fclose(f2);

}

void file_cesar_decryption(usint k, char *p1, char *p2){

    message("Opening two files ...", 0);

    FILE *f1 = fopen(p1, "rb");
    FILE *f2 = fopen(p2, "wb");

    message("Done !", 0);

    if(f1){

        message("Source file [OK]", 1);

        rewind(f1);

        if(f2){

            message("Destination file is created [OK]", 1);
            message("Decrypting file ...", 0);

            usint BUFFER[BUFFER_SIZE];
            int _length = 0;

            while((_length = fread(BUFFER, sizeof(usint), BUFFER_SIZE, f1)) > 0){
                cesar_decrypted_values(k, _length, BUFFER);
                fwrite(BUFFER, sizeof(usint), _length, f2);
            }

            message("Done !", 0);
        }
        else{
            message("Destination file is not created [ERROR]", 2);
        }
    }
    else{
        message("Source file [ERROR]", 2);
    }

    fclose(f1);
    fclose(f2);

}

/* Xa pour afficher les différentes messages */

void message(char* m, short int v){
    switch(v){
        case 0:
            color(15, 0);
            break;
        case 1:
            color(10, 0);
            break;
        case 2:
            color(12, 0);
            break;
    }

    printf("%s\n", m);

    color(15, 0);
}

void color(int x, int y){
    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(h, y * 16 + x);
}
